package com.muchau.jokeactivity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class JokeActivity extends AppCompatActivity {

    public static final String JOKE_PARAM = "joke";

    TextView mJokeTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_joke);

        Intent intent = getIntent();
        if (intent.hasExtra(JOKE_PARAM)) {
            String joke = intent.getStringExtra(JOKE_PARAM);
            mJokeTextView = findViewById(R.id.jokeTextView);
            mJokeTextView.setText(joke);
        }

    }
}