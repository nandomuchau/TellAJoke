package com.muchau.jokeslib;

import org.junit.Test;

/**
 * Created by Luis F. Muchau on 8/29/2018.
 */
public class JokerTest {

    @Test
    public void test() {
        Joker joker = new Joker();
        assert joker.getJoke().length() != 0;
    }
}