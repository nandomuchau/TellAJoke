package com.muchau.tellajoke;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.muchau.tellajoke.MainActivityFragment;

/**
 * A placeholder fragment containing a simple view.
 */
public class JokeFragment extends MainActivityFragment {

    protected InterstitialAd mInterstitialAd;

    public JokeFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_main, container, false);

        mInstTextView = root.findViewById(R.id.instructions_text_view);
        mJokeButton = root.findViewById(R.id.tellJoke_button);

        mLoadingBar = root.findViewById(R.id.progress_bar);
        mLoadingBar.setVisibility(View.INVISIBLE);

        mJokeButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                if (mInterstitialAd.isLoaded()){
                    mInterstitialAd.show();
                }else {
                    tellJoke();
                }
            }
        });

        addAd(root);
        addInterstitialAd();

        return root;
    }

    public void addAd(View root){
        AdView mAdView = root.findViewById(R.id.adView);
        // Create an ad request. Check logcat output for the hashed device ID to
        // get test ads on a physical device. e.g.
        // "Use AdRequest.Builder.addTestDevice("ABCDEF012345") to get test ads on this device."
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .build();
        mAdView.loadAd(adRequest);
    }

    public void addInterstitialAd() {
        MobileAds.initialize(getActivity(), "ca-app-pub-3940256099942544~3347511713");

        mInterstitialAd = new InterstitialAd(getActivity());
        mInterstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                tellJoke();
            }
        });
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();

        mInterstitialAd.setAdListener(null);
    }
}
