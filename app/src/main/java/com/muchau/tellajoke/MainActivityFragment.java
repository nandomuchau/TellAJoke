package com.muchau.tellajoke;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.muchau.jokeactivity.JokeActivity;


/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment implements AsyncResponse{

    protected TextView mInstTextView;

    protected Button mJokeButton;

    protected ProgressBar mLoadingBar;

    private EndpointsAsyncTask asyncTask;

    public MainActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_main, container, false);

        mInstTextView = root.findViewById(R.id.instructions_text_view);
        mJokeButton = root.findViewById(R.id.tellJoke_button);

        mJokeButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                tellJoke();
            }
        });

        mLoadingBar = root.findViewById(R.id.progress_bar);
        mLoadingBar.setVisibility(View.INVISIBLE);

        return root;
    }

    public void tellJoke(){
        updateLoading(true);

        asyncTask = new EndpointsAsyncTask(this);
        asyncTask.execute();
    }

    public void updateLoading(boolean loading){
        if (loading){
            mInstTextView.setVisibility(View.INVISIBLE);
            mJokeButton.setVisibility(View.INVISIBLE);
            mLoadingBar.setVisibility(View.VISIBLE);
        } else {
            mInstTextView.setVisibility(View.VISIBLE);
            mJokeButton.setVisibility(View.VISIBLE);
            mLoadingBar.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void processJokes(String out) {
        if (getActivity() == null) {
            return;
        }

        Intent intent = new Intent(getActivity(), JokeActivity.class);
        intent.putExtra(JokeActivity.JOKE_PARAM, out);
        startActivity(intent);
    }

    @Override
    public void onStart(){
        super.onStart();
        if (asyncTask == null || asyncTask.getStatus() == AsyncTask.Status.FINISHED){
            updateLoading(false);
        }
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
        mJokeButton.setOnClickListener(null);
        if (asyncTask != null){
            asyncTask.cancel(true);
        }
    }
}
