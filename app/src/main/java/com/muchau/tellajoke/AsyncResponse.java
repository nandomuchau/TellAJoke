package com.muchau.tellajoke;

public interface AsyncResponse {
    void processJokes(String out);
}
