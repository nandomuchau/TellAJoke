package com.muchau.tellajoke;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.text.TextUtils;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.concurrent.CountDownLatch;

import static junit.framework.Assert.assertFalse;
import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class JokeInstrumentedTest {

    private Context mContext;

    private CountDownLatch countDownLatch;

    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.muchau.tellajoke.free", appContext.getPackageName());
    }

    @Test
    public void testAsyncTask () throws InterruptedException {
        assertTrue(true);
        mContext = InstrumentationRegistry.getContext();
        countDownLatch = new CountDownLatch(1);
        new EndpointsAsyncTask(new AsyncResponse(){

            @Override
            public void processJokes(String out) {
                assertFalse(TextUtils.isEmpty(out));
                countDownLatch.countDown();
            }
        }).execute();
        countDownLatch.await();
    }
}
